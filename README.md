# Overview

This repository is meant to be an introduction to Immutable Objects and Functional Programming

## Viewing the Presentation

To view the presentation locally, install [reveal-md](https://github.com/webpro/reveal-md).

```bash
npm install -g reveal-md
reveal-md notes.md --vertical-separator='--'
```

## Concepts

Further concepts / topics related to Functional Programming

- Monads
- Functors
- Lenses
- Immutability

## Links

The extra reading materials are included below for convenience

### General Concepts

- [Functors, Applicatives, And Monads In Pictures](http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html) - Great, intuitive article on Monads
- [6 Benefits of Programming with Immutable Objects](https://www.linkedin.com/pulse/20140528113353-16837833-6-benefits-of-programming-with-immutable-objects-in-java) - General article on Immutability
- [Factory Pattern](https://en.wikipedia.org/wiki/Factory_%28object-oriented_programming%29) - Wikipedia page on the Factory Pattern
- [Builder Pattern](https://en.wikipedia.org/wiki/Builder_pattern) - Wikipedia page on the Builder Pattern
- [Memoization - Wikipedia](https://en.wikipedia.org/wiki/Memoization) - Wikipedia page on Memoization
- [Basic Lensing](https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/basic-lensing) - Article on lensing

### Language Specific

#### Haskell

- [Learn You a Haskell](http://learnyouahaskell.com/functors-applicative-functors-and-monoids) - Awesome introduction to Haskell
- [Real World Haskell](http://book.realworldhaskell.org/read/) - A 'heavier' introduction, with great content

#### Java

- [Immutables](http://immutables.github.io/) - A great library for generating immutable Objects
- [Guava](https://github.com/google/guava/wiki/Release21) - Google library with great Collection utilities
- [MutabilityDetector4FindBugs](https://github.com/MutabilityDetector/MutabilityDetector4FindBugs) - Tool for assuring immutability is preserved
- [Functional Java](http://www.functionaljava.org/) - Library for Functional Programming in Java
- [Immutable Objects (The Java™ Tutorials > Essential Classes > Concurrency)](https://docs.oracle.com/javase/tutorial/essential/concurrency/immutable.html) - Oracle Documentation on Immutable Objects