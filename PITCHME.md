## Functional Programming & Immutability

---

## What Is It?

Doesn't my programming function just fine?

Why do you want to mute me?

+++

### Functional Programming

> "In computer science, functional programming is a programming paradigm—a style of building the structure and elements of computer programs—that treats computation as the evaluation of mathematical functions and avoids changing-state and mutable data."—[Wikipedia](https://en.wikipedia.org/wiki/Functional_programming)

+++

### Immutability

> "In object-oriented and functional programming, an immutable object (unchangeable[1] object) is an object whose state cannot be modified after it is created.[2] This is in contrast to a mutable object (changeable object), which can be modified after it is created."—[Wikipedia](https://en.wikipedia.org/wiki/Functional_programming)

+++

> It's a UNIX system! I know this!

```java
String s = "Hello";
s = s + " World";
Integer i = 1;
```

---

## Why?

The path to Nirvana - ~~desire~~ mutability is suffering

Benefits:

- Concurrency
- Performance
- Reasonability / Analysis
- Quality

+++

Some of the first things we learn:

- **globals** are bad
- **constants** are good

+++

But...

+++

Fields on a class are:

- Globals with a slightly smaller scope
- Mutable (I.e. not constant)

---

### Equality for all 

- Immutable objects can be used by many threads without risk of race conditions
- Easy, cheap concurrency is a natural outcome of immutability

+++

### Performance Enhancing Code

Runtime efficiency can be can be greatly enhanced

Not just in theory, but in fact! - [Guava Immutables compared to vanilla Java](https://github.com/DimitrisAndreou/memory-measurer/blob/master/ElementCostInDataStructures.txt)

+++

### Memoization

(No, not the trick we used to get through exams)

Optimization technique, done by caching the results of an expensive function call

+++

### Reasonability (Y U DO DIS?)

- It is **much** easier to reason about something which doesn't change - you don't need to go hunting down the modifiers of state
- This surety in immutability can be enhanced via static analyzers like [MutabilityDetector4FindBugs](https://github.com/MutabilityDetector/MutabilityDetector4FindBugs)

---

## So, Why Do We Still Use Mutable State?

+++

- Old habits die hard
- The first rule of programing club is... there are no rules
- Humans are lazy
    - ...and programmers are lazy humans
- It's unavoidable 😢
    - There is an unavoidable need to modify state
    - But fight the power! Avoid the Appeal to Futility Fallacy!

---

## How To Shot Web? (Java Style)

![take my money](./docs/tmm.jpg)

+++

In some ways, we already are:

- java.lang.{String,Integer,Byte,...}
- java.math.{BigInteger,BigDecimal}

+++

Starting small

- Make all fields private
- Don't provide mutators
- Make the class final (Strong Immutability)
  - Or make methods final (Weak Immutability)

+++

Pareidolia - Seeing patterns

- Use [Factory](https://en.wikipedia.org/wiki/Factory_%28object-oriented_programming%29) Pattern
- Use [Builder](https://en.wikipedia.org/wiki/Builder_pattern) Pattern (Such as the one provided by [Immutables](https://immutables.github.io/immutable.html#builder))

+++

Mind Your Step

Be careful not to create bloated constructors!

```java
new NoteBean(
  note.getId(), note.getType(), note.getCategoryGroup(),
  note.getCategoryName(), note.getTopic(), note.getSummary(),
  note.getContent(), note.getContentType(), note.getAlerts(),
  null, note.getLastUpdated(),
  note.getUpdatedBy() != null ?
    note.getUpdatedBy().getUsername() : null,
                                        note.isSensitive(),
  regarding, source, checkListItemValue)
```

<iframe width="280" height="155" src="https://www.youtube.com/embed/ZdboqdvP8SE" frameborder="0" allowfullscreen></iframe>

+++

> "Classes should be immutable unless there's a very good reason to make them mutable....If a class cannot be made immutable, limit its mutability as much as possible." - Joshua Bloch in [Effective Java, 2nd Edition](https://www.amazon.com/exec/obidos/ASIN/0321356683/ref=nosim/javapractices-20)

---

## A Veritable Cornucopia of (Dys)Functional programming

---

### Functors (And their children)

http://learnyouahaskell.com/functors-applicative-functors-and-monoids

+++

### Right in the monads

[Monads](https://en.wikipedia.org/wiki/Monad_%28functional_programming%29) are good at Capturing State

+++

#### Monad Laws

```haskell
do { f x } ≡ do { v <- return x; f v }
do { m } ≡ do { v <- m; return v }
do { x <- m; y <- f x; g y } ≡ do { y <- do { x <- m; f x }; g y }
```

+++

### Delicious Currying

Break down functions into a chain of functions, each taking one argument.

```
f(x, y) → f(x(y))
```

+++

## A Simple Example

```js
function sum(a,b) => a + b

// Curried:
function sumb(b) => (+) b
function suma(a) => sumb(a)
```

---

## Lenses - More than just Grandma's glasses
[Basic Lensing](https://www.schoolofhaskell.com/school/to-infinity-and-beyond/pick-of-the-week/basic-lensing)

---

## Additional Reading

+++

### General

- [Memoization - Wikipedia](https://en.wikipedia.org/wiki/Memoization)
- [6 Benefits of Programming with Immutable Objects](https://www.linkedin.com/pulse/20140528113353-16837833-6-benefits-of-programming-with-immutable-objects-in-java)

+++

### Java Libraries and Tutorials

- [Immutable Objects (The Java™ Tutorials > Essential Classes > Concurrency)](https://docs.oracle.com/javase/tutorial/essential/concurrency/immutable.html)
- [Immutables](http://immutables.github.io/)
- [Guava](https://github.com/google/guava/wiki/Release21)
- [MutabilityDetector4FindBugs](https://github.com/MutabilityDetector/MutabilityDetector4FindBugs)
- [Functional Java](http://www.functionaljava.org/)